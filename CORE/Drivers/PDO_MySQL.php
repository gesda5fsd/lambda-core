<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE\Drivers;

/**
 * CORE class PDO MySQL driver
 *
 * @category CORE_Drivers
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class PDO_MySQL
{

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->PDO = new \PDO(
            "mysql:dbname={$_ENV['MYSQL_DATABASE']};host={$_ENV['MYSQL_HOSTNAME']}",
            $_ENV['MYSQL_USERNAME'],
            $_ENV['MYSQL_ROOT_PASSWORD']
        );
    }

    /**
     * [__call description]
     * @param  [type] $method [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function __call($method, $params)
    {
        return call_user_func_array([$this->PDO, $method], $params);
    }
}
