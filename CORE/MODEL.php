<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE;

/**
 * CORE class MODEL
 *
 * @category CORE_Class
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class MODEL extends \CORE\Library\ORM
{

    public $table = 'table_name';

    private $standart_fields = [
        'created' => ['type' => 'DATETIME', 'null' => false, 'editable' => false, 'sort' => -3],
        'updated' => ['type' => 'DATETIME', 'null' => true,  'editable' => false, 'sort' => -2],
        'id'      => [
            'type'     => 'INT',
            'size'     => '(11)',
            'null'     => false,
            'options'  => 'AUTO_INCREMENT',
            'listable' => false,
            'editable' => false,
            'sort'     => -4
        ],
        'status'  => [
            'type'      => 'INT',
             'size'     => '(1)',
             'null'     => false,
             'default'  => 0,
             'listable' => false,
             'sort'     => -1,
             'rule'     => 'status',
             'required' => true
        ]
    ];

    private $standart_keys = [
        'pk' => ['id']
    ];

    public $fields = [];

    public $keys = [];

    /**
     * Model constructor.
     * @param array $config Config parameters.
     */
    public function __construct(Array $config = [])
    {
        parent::__construct();
        foreach ($config as $key => $value) {
            $this->{$key} = $value;
        }

        $this->fields = array_merge($this->fields, $this->standart_fields);
        $this->keys   = array_merge($this->keys,   $this->standart_keys);

        // $this->collection = new \CORE\Library\ORM($this->table, $this->fields);
    }

    /**
     * [getFields description]
     * @return [type] [description]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * [save description]
     * @param  array $collection [description]
     * @return [type]            [description]
     */
    public function save(Array $collection = [])
    {
        $id = $collection['id'] ?? false; // save the id

        foreach ($collection as $field => $value) {
            if (isset($this->fields[$field]['editable'])
            && !      $this->fields[$field]['editable']) {
                unset($collection  [$field]);
            }
            switch   ($this->fields[$field]['rule']) {
                case 'password':
                    if (empty($collection[$field])) {
                        unset($collection[$field]);
                        continue;
                    }
                    $collection[$field] = hash(SHA512, $collection[$field]);
                    break;
            }
        }
        if ($id) {
            $collection['id'] = $id;
        }
        return parent::save($collection);
    }
}
