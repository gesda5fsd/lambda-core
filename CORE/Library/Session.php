<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE\Library;

define('__MIXED__', ['__mixed__' => []]);

/**
 * CORE class Session
 *
 * @category CORE_Library
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class Session
{

    /**
     * [__construct description]
     */
    public function __construct()
    {
        session_start();
    }

    /**
     * [destruct description]
     * @return [type] [description]
     */
    public function destruct()
    {
        session_destroy();
    }

    /**
     * [userdata description]
     * @param  string  $fetch [description]
     * @param  boolean $data  [description]
     * @return [type]         [description]
     */
    public function userdata(String $fetch = '', $data = __MIXED__)
    {
        if ($data == __MIXED__) {
            return $_SESSION[$fetch];
        }
        $_SESSION[$fetch] = $data;
    }

    /**
     * [userdata description]
     * @param  string  $fetch [description]
     * @param  boolean $data  [description]
     * @return [type]         [description]
     */
    public function flashdata(String $fetch = '', $data = __MIXED__)
    {
        $fetch = "λ_{$fetch}";

        if ($data == __MIXED__) {
            $data = $_SESSION[$fetch];
            unset($_SESSION[$fetch]);
            return $data;
        }
        $_SESSION[$fetch] = $data;
    }
}
