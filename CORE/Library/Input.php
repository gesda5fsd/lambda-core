<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE\Library;

use \CORE\λ as λ;

/**
 * CORE class INPUT
 *
 * @category CORE_Library
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class INPUT
{

    /**
     * Class constructor.
     **/
    public function __construct()
    {
        foreach (Security::getRequestData() as $key => $value) {
            $this->{$key} = $value;
        }
        return $this;
    }
}
