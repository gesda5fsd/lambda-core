<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE\Library;

/**
 * CORE class MIME
 *
 * @category CORE_Library
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class MIME
{

    /**
     * [type description]
     * @param  boolean $extention [description]
     * @return [type]             [description]
     */
    public static function type(String $extention)
    {
        switch ($extention) {
            case 'css':
                header('Content-Type: text/css');
                break;

            case 'min.js':
            case 'js':
                header('Content-Type: application/javascript');
                break;

            case 'svg':
                header('Content-Type: image/svg+xml');
                break;

            case 'png':
                header('Content-Type: image/png');
                break;

            case 'woff2':
            case 'woff':
            case 'ttf':
                header("Content-Type: application/font-{$extention}");
                break;

            default:
                break;
        }
    }
}
