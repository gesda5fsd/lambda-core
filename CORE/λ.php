<?php

namespace CORE;

/**
 * PHP λ::lambda(); // Lightweight PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

class λ
{

    public $routes = [];

    public $request = null;

    /**
     * [__construct description]
     * @param [type] $configs [description]
     */
    public function __construct($configs = null)
    {
        if (is_array($configs)) {
            foreach ($configs as $config => $value) {
                $this->{$config} = $value;
            }
        } else {
            $this->request = $configs;
        }
        $this->Bootstrap();
    }

    /**
     * [Bootstrap description]
     */
    public function Bootstrap()
    {
        spl_autoload_register(function ($classpath = null, $hold = false) {
            $module = str_replace('\\', DIRECTORY_SEPARATOR, "{$classpath}.php");
            try {
                if (class_exists($classpath)) {
                    return;
                }
                if (substr($module, 0, 5) === 'HOLD/') {
                    $module = substr($module, 5);
                    $hold   = $classpath;
                }
                if (!file_exists($module)) {
                    throw new \Exception("\e[1mλ\e[0m File `{$module}` does not exists", 1);
                }

                include_once $module;

                if ($hold) {
                    $class          = substr($hold, 1 + strrpos($hold, '\\'));
                    $namespace      = substr($hold, 0,  strrpos($hold, '\\'));
                    $true_namespace = substr($hold, 1 + stripos($hold, '\\', 1));

                    class_alias('\CORE\Hold', $classpath);
                    $classpath::$class = $true_namespace;
                    // That was the original idea.
                    //
                    // eval("namespace $namespace;
                    //     class $class extends \CORE\Hold
                    //     {
                    //         public \$class = '\\$true_namespace';
                    //     }");
                }
                if (!class_exists($classpath)) {
                    throw new Exception("Class `{$classpath}` does not exist.", 1);
                }
            } catch (Exception $e) {
                echo "\n\n\nλ An error ocurred: {$e->getMessage()}\n\n\n";
            }
        });
    }

    /**
     * [route description]
     * @param  [type] $route  [description]
     * @param  [type] $target [description]
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
    public function route($route, $target = null, $method = null)
    {
        $this->routes[$route] = [$target, $method];
        return $this;
    }

    /**
     * [__destruct description]
     */
    public function __destruct()
    {
        $request = $this->routes[$this->request] ?? $this->showError("404 Page not found: {$this->request}", 404);

        if (is_object($request[0])) {
            $class = get_parent_class($request[0]) ? $request[0]->__instance() : $request[0];
            if (!empty($request[1])) {
                return $class->{$request[1]}();
            }
            if (!empty($class->method)) {
                return $class->{$class->method}();
            }
            switch (get_parent_class($class)) {
                default:
                    $request = get_class($class);
                    echo "$request é instância de ".get_parent_class($class)."\n";
                    break;
            }
        }
        echo $request[0];
    }

    /**
     * [showError description]
     * @param  string  $message [description]
     * @param  integer $code    [description]
     * @return [type]           [description]
     */
    public function showError($message = '', $code = 200)
    {
        header("");
        echo $message;
        exit;
    }
}
