<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE;

/**
 * CORE class VIEW
 *
 * @category CORE_Class
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class VIEW
{

    public $template;

    public $templates;

    public $folder;
    /**
     * Model constructor.
     * @param array $config Config parameters.
     */
    public function __construct(Array $config = [])
    {
        // $this->set($config);
    }


    /**
     * [set description]
     * @param array $config [description]
     * @return Void
     */
    public function set(Array $config = [])
    {
        foreach ($config as $key => $value) {
            $this->{$key} = $value;
        }
        return $this;
    }


    /**
     * [render description]
     * @param Array $data config data
     * @return [type] [description]
     */
    public function render($data = [])
    {
        foreach (array_merge((array) $this, $data) as $key => $value) {
            $$key = $value;
        }

        ob_start();

        $source = str_replace('//', '/', "{$this->templates}/{$template}.php");

        if (!file_exists($source)) {
            return false;
        }
        include $source;


        $output = ob_get_contents();
        ob_end_clean();

        $elapsed = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        return str_replace('{elapsed_time}', $elapsed, $output);
    }
}
