<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE;

/**
 * CORE class Holder
 *
 * @category CORE_Class
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class Hold
{

    public static $class = null;

    public $instance = null;

    public $method = null;

    /**
     * [__construct description]
     * @param [type] $method [description]
     */
    public function __construct($method = null)
    {
        $this->method = $method;
    }

    /**
     * [__call description]
     * @param  [type] $method [description]
     * @param  [type] $arg    [description]
     * @return [type]         [description]
     */
    public function __call($method = null, $arg = null)
    {
        if (!$this->instance) {
            $this->getInstance();
        }
        return $this->instance->{$method}($arg);
    }

    /**
     * [getInstance description]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */
    public function getInstance($parameters = null)
    {
        if ($this->instance == null) {
            $this->instance = new SELF::$class($parameters);
        }
        return $this->instance;
    }
}
